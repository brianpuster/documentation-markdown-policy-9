title: Requisition and Salary Adjustment Policy
author: Julie Storm, Chief People Officer
published: 2019-11-26

#### Policy level:
Very Important

#### Data Classification:
Internal

#### Approver(s):
- Senior Finance
- Julie Storm, Chief People Officer

#### Applicable Locations:
- All

#### Effective date:
- 2019-11-26

#### Last Update:
- 2020-09-03

---

##### Policy text:

**Hiring Requisitions**

All requisitions for open positions, including new positions, backfills and the transitioning of a consulting position to a full-time team member position will be completed by the hiring manager or the ELT leader using the [requisition form](https://theloop.constructconnect.com/Interact/Pages/Content/Document.aspx?id=4579) found on the Loop. The requisition must be completely filled out, including the salary range for the position. Once completed, the requisition goes to Talent Acquisition, who will validate the requested salary range with the Director, P&C via PayFactors. If the salary range is in question, Talent Acquisition will have a discussion with the hiring manager to reach an agreeable range. Talent Acquisition then sends an e-mail to the EVP, Finance and CEO with the ELT leader cc’d to approve the request. Requisitions will not be posted until all pertinent approvals are obtained. Once obtained, the requisition will be posted within 24 hours.

Current exceptions as of the date of this policy are BDR roles for Trade Contractor and Content Specialist roles for Content. This policy will be updated with any changes to this exception.

If a requisition needs more clarification, the CEO or EVP, Finance will reach out to all included on the approval e-mail.  If the requisition is not approved, this will also be communicated by both the CEO and EVP, Finance via e-mail to those on the approval e-mail.

A previously approved requisition may be cancelled by either the CEO, EVP Finance, applicable ELT Leader or VP Level Leader or the hiring manager. In the event this occurs, all relevant parties will be notified via email by the person cancelling in a timely manner.

An approved requisition will remain in effect until the earlier of: i) acceptance of an approved offer letter, ii) requisition is cancelled, or iii) 90 days from approval date.

**Salary Adjustments**

All requests for salary adjustments (i.e. not part of the annual salary increase process) must follow the guidelines outlined below. All salary adjustment requests must also align with the most recent financial forecast. [See Headcount Forecasting Approval Policy](https://policydocs.buildone.co/pages/finance/Finance/FP&A%20Processes/Headcount%20Approval%20Process)

This includes market adjustments, off-cycle increases, transitions to new roles and promotions.

###### In Cycle Adjustments (preferred approach / timing)

The process begins by receiving team-specific pay adjustment workbooks from Finance in early March of each year. Team members with a last pay adjustment or hire date after October 1st of the prior year will not be eligible for a current year In Cycle adjustment. The workbooks will include an allowance amount for each senior leader to allocate across their respective team members. By March end, the returned workbooks will be reviewed and validated to be within the assigned allowance. Pay adjustments resulting in a team exceeding their assigned allowance will need to apply the Off Cycle approval process described below to the team member(s) receiving the largest adjustment.

All In Cycle Adjustments will be posted to Ultipro in early April with an effective date of April 1st to be included in the April 15th pay period.

###### Off Cycle Adjustments (only to be used in exceptional business cases)

The process begins with the team member’s manager submitting the request by completing the Salary Adjustment form found on the Loop. The Director, P&C will validate the request through the use of Payfactors and/or internal equity analysis.  If the salary range is in question, Talent Acquisition will have a discussion with the hiring manager to reach an agreeable range Once a range is agreed upon, the form will then be sent to the Chief People Officer (CPO) and ELT Leader or VP Level Leader, as well as the EVP, Finance and EVP for approval. The hiring manager is copied.  During the final approval process, additional information may be requested of Talent Acquisition. Once final approvals are received by Talent Acquisition, they will notify the hiring manager to proceed with the verbal offer. Talent Acquisition is to be notified when this has been completed.

Salary adjustments related to role transitions or promotions will require a new offer letter. Talent Acquisition will create the offer letter once the hiring manager confirms a verbal offer has been extended to the affected team member. The offer letter will be sent via DocuSign by Talent Acquisition to the team member and hiring manager.

Please Note: The entire salary adjustment approval cycle must be completed before the hiring manager is authorized to inform the affected team member of any potential adjustment.  In keeping with our value of respect, there is to be no expectation set or promise made regarding the approval of the promotion to the team member prior to or during the approval cycle.

**Offer Letters**

Internal offer letters will be handled as described above. External offer letters will be created by Talent Acquisition and, if financial terms (salary, bonus, relocation and/or recruiting fee) associated with the offer exceeds the range authorized by the approved requisition, an email will be sent to EVP and EVP, Finance seeking approval of all deviations. To assist them in considering approval of the proposed financial terms, a copy of the signed requisition will be sent. The EVP and EVP, Finance will then either approve, deny, seek additional information or propose changes to the request via email to Talent Acquisition. Once the financial terms are approved, Talent Acquisition will send the offer letter to the candidate and copy the hiring manager. Talent Acquisition will liaise with the candidate to arrive at agreed-upon terms within the approved salary range. Talent Acquisition will coordinate with the hiring manager and the EVP and EVP, Finance if additional approvals are required to successfully complete the hire. Further, CEO approval will be required for all external offer letters to successful candidates who are also former team members where prior employment with the Company termed within the prior 24 months.

For external offer letters with financial terms within the ranges of the approved requisition, Talent Acquisition does not need to obtain approval by the Executive Vice President.