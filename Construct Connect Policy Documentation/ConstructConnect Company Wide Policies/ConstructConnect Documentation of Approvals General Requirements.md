title: Documentation of Approvals – General Requirements
author: Steve Testa - VP Strategic Initiatives and Continuous Improvement
published: 2020-02-05
effective date: 2020-02-05
 
 
#### Policy Level
- Important Not Urgent
 
#### Approver(s)
-  Dave Storer - Controller

#### Location or Team Applicability
- All 

---
 
#### Policy text:
This policy covers the general requirements for documentation of any an all types of approvals.
 
- All approvals must be documented in writing/electronic text
- There are no verbal approvals
- If a request for approval is not responded to, approval cannot be implied. 
- Approvals must be attached to the content (communication, policy, etc..) being approved and stored in a central location that is appropriate for the team and/or content. (Shared drive, group email box, etc.)

---
