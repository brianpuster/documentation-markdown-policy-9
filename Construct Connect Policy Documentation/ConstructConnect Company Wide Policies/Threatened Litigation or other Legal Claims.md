title: Notification of Threatened Litigation or other Legal Claims
author: Jeff Cryder, EVP
published: 2020-08-21

#### Policy level:

* Very Important

#### Data Classification:

* Internal

#### Approver(s):

* Buck Brody, EVP Finance

* Dave Conway, President

#### Location or Team Applicability:

* All

#### Effective date:

* August 1, 2020

#### Policy text:

**Background**

A legal threat is a statement by a party that it intends to take legal action on another party, generally accompanied by a demand that the other party take an action demanded by the first party or refrain from taking or continuing actions objected to by the demanding party.

Legal threats take many forms. Common to all is a party making a threat to take some form of action of a legal nature. Most common is the threatened initiation of a lawsuit against a second party. Other threats might include an administrative law action or complaint, referring a party to a regulatory body, turning a party into the legal authorities over a crime or civil infraction, or the like. Legal threats are often veiled or indirect, e.g. a threat that a party "shall be forced to consider its legal options" or "will refer the matter to legal counsel." Two most common types of threats are:

> *Cease and Desist*

> : A cease and desist notice is a formalized legal demand that a party stop ("cease") and refrain ("desist") from an activity that the demanding party finds objectionable, generally couched in formal language accusing the activity of violating the law.

> : The objected-to activity may be most anything, although cease-and-desist notices are particularly common among certain areas of the law:

> > * alleged intellectual property infringement (e.g. patent infringement, trademark infringement, copyright infringement, etc.)

> > * alleged defamation such as libel and slander

> > * harassment, nuisance, and other torts

> > * violation of certain agreements to not engage in certain commercial conduct (e.g. with respect to competition, territories, etc.)

> *Demand Letter*

> : A "demand letter" is a formalized demand by a party that another party pay money or take certain acts, often accompanied by a claim that the second party has engaged in illegal conduct, with an implicit or explicit threat that non-compliance will lead the demanding party to take legal action.

**Instructions**

Upon receipt of any verbal or written notice from a third-party indicating their intent to take legal action against ConstructConnect or an affiliated Roper company, you are to i) forward the written notice to Mr. Cryder EVP or ii) if the notice is received verbally, you are to summarize the nature of the conversation in an email to Mr. Cryder EVP. The email summary is to include the nature and scope of the complaint as well as any facts provided by the third-party in support of their claim. You are not to research or investigate the veracity of the claim prior to notification to Mr. Cryder.

Mr. Cryder will coordinate next steps after consultation with other members of senior leadership and Roper legal counsel, as applicable.